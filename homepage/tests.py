# from django.test import TestCase , Client, LiveServerTestCase
# from django.http import HttpRequest
# from django.contrib.auth.models import User
# from django.contrib.auth import authenticate, login
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.support.ui import WebDriverWait

# import os
# import time

# # Create your tests here.
# class LoginpageUnitTest(TestCase):
#     def test_login_page(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 302)
    
#     def test__request_api(self):
#         response = Client().get('/api/v1/login/')
#         self.assertEqual(response.status_code, 400)

#         response = Client().get('/api/v1/signup/')
#         self.assertEqual(response.status_code, 400)

# class LoginpageFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         super(LoginpageFunctionalTest, self).setUp()

#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument("--headless")
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('disable-gpu')

#         if 'GITLAB_CI' in os.environ:
#             self.browser = webdriver.Chrome(
#                 './chromedriver', chrome_options=chrome_options)
#         else:
#             self.browser = webdriver.Chrome(chrome_options=chrome_options)        

#     def tearDown(self):
#         self.browser.quit()
#         super(LoginpageFunctionalTest, self).tearDown()

#     def test_login_in_loginpage(self):
#         User.objects.create_user('humas', 'humhums@gmail.com', 'humas1503')
#         self.browser.get(self.live_server_url + '/')
 
#         time.sleep(5)

#         username = self.browser.find_element_by_id("username")
#         username.send_keys("humas")

#         password = self.browser.find_element_by_id("password")
#         password.send_keys("humas1503")

#         login_button = self.browser.find_element_by_id("logInButton")
#         login_button.click()

#         time.sleep(5)
#         self.assertIn('humas', self.browser.page_source) 

#         logout_button = self.browser.find_element_by_class_name("logout-button")
#         logout_button.click()

#         time.sleep(5)
#         self.assertNotIn('humas', self.browser.page_source)

#     def test_login_page_if_login_no_user(self):
#         self.browser.get(self.live_server_url + '/')

#         # Wait until page open 
#         time.sleep(5)

#         username = self.browser.find_element_by_id("username")
#         username.send_keys("marshaler")

#         password = self.browser.find_element_by_id("password")
#         password.send_keys("humas1503")

#         login_button = self.browser.find_element_by_id("logInButton")
#         login_button.click()

#         time.sleep(5)

#         self.assertIn('Username tidak terdaftar, mohon periksa kembali dan coba lagi', self.browser.page_source) # Verify error message is showed
    
#     def test_login_page_if_wrong_password(self):
#         # Create User
#         User.objects.create_user('humas', 'humhums@gmail.com', 'humas1503')
#         self.browser.get(self.live_server_url + '/')

#         # Wait until page open 
#         time.sleep(5)
#         username = self.browser.find_element_by_id("username")
#         username.send_keys("humas")
#         password = self.browser.find_element_by_id("password")
#         password.send_keys("humas15034")
#         login_button = self.browser.find_element_by_id("logInButton")
#         login_button.click()
#         time.sleep(5)

#         self.assertIn('Username / password salah, mohon periksa kembali dan coba lagi', self.browser.page_source)
    
#     def test_create_acc(self):
#         self.browser.get(self.live_server_url + '/')

#         #  page open 
#         time.sleep(5)
#         sign_up_button = self.browser.find_element_by_id("createAccountLink")
#         sign_up_button.click()

#         time.sleep(5)

#         # Fill details
#         username = self.browser.find_element_by_id("username1")
#         username.send_keys("humas")
#         email = self.browser.find_element_by_id("email1")
#         email.send_keys("humhum@gmail.com")
#         password = self.browser.find_element_by_id("password1")
#         password.send_keys("humas1590")

#         # Sign up
#         sign_up_button = self.browser.find_element_by_id("signUpButton")
#         sign_up_button.click()

#         time.sleep(5)

#         user = authenticate(username="humas", password="humas1590")
#         self.assertTrue(user) # User None
#         sign_up_button = self.browser.find_element_by_id("createAccountLink")
#         sign_up_button.click()

#         time.sleep(5)

#         # TEST DUPLICATE USERNAME
#         username = self.browser.find_element_by_id("username1")
#         username.clear()
#         username.send_keys("humas")
#         email = self.browser.find_element_by_id("email1")
#         email.clear()
#         email.send_keys("humhum@gmail.com")

#         password = self.browser.find_element_by_id("password1")
#         password.clear()
#         password.send_keys("humas1590")

#         # Sign up
#         sign_up_button = self.browser.find_element_by_id("signUpButton")
#         sign_up_button.click()

#         time.sleep(5)

#         self.assertIn('Username sudah dipakai, coba ganti dengan username lain', self.browser.page_source)
